# Spamhaus API

Product management just came back from a security conference and saw people using free
threat intel sources to do detections. Using DNS requests, you can send an IP address in
reverse notation to determine whether it is on a block list or not.

Naturally, they want us to be able to query these servers and provide answers to our customers
on whether or not an IP is malicious.

Given an IP address of 1.2.3.4 we can look that up at Spamhause by issuing the following
commands (just an example):
```bash
host 4.3.2.1.zen.spamhaus.org
```
or
```bash
dig 4.3.2.1.zen.spamhaus.org
```
# Project Overview

The goal of this project is to accept a list of IP addresses and to check the against the Spamhaus blocklists to determine if they are malicious or not. The IP addresses and their Spamhaus response codes are then stored in a database. If an IP address being looked up already exists in the database, then it will be checked again and the Spamhaus reponse codes will be updated.

## Project Architecture

* golang 1.16
* go mod, go sum
* graphQL API
* SQLite DB
* Docker

## External Libraries Used

* [`github.com/go-chi/chi/v5`](https://github.com/go-chi/chi/v5) - A good router. I mainly used it for its middleware
* [`github.com/mattn/go-sqlite3`](https://github.com/mattn/go-sqlite3) - simple sqlite driver
* [`github.com/99designs/gqlgen`](https://github.com/99designs/gqlgen) - graphQL golang integration
* [`github.com/golang/mock`](https://github.com/golang/mock) - used for mocking a database to test graphQL api
* [`github.com/stretchr/testify`](https://github.com/stretchr/testify) - great for code testing
* [`github.com/google/uuid`](https://github.com/google/uuid) - uuid generator

# Set Up
## Clone from source
Use the following command to clone the repository:
```bash
╰─$ git clone git@gitlab.com:davidheadrick93/spamhaus-api.git
```
Then cd into the directory. Type the following command:

```bash
#download all dependencies
╰─$ go get -d -v ./...
```
## Run locally
Type the following command to run locally:

```bash
#run the server
╰─$ go run server.go
```

If it is up and running you should see this:

```bash
╰─$ go run server.go
2021/05/04 15:20:00 Opening DB File: ./test/test.db
```

Once you have cloned the repository and installed all dependencies - visit [`http://localhost:8080/graphql`](http://localhost:8080/graphql) to get into the graphQL playground.

The application is protected by basic authentication. The username and password are:

```
username: secureworks
password: supersecret
```

# GraphQL Examples
A GraphQL playground enpoint is provided with the application at `/graphql`. For testing, a mutation and a query are provided. The test data included is a test IP provided by Spamhaus (127.0.0.1) that is not listed on their blocklists and a known spam IP address registered with Spamhaus (23.129.64.232).

```graphql
mutation Enqueue {
  enqueue(input:["127.0.0.1", "23.129.64.232") 
}

query GetIPDetails {
  getIPDetails(input: "23.129.64.232") {
    uuid
    created_at
    updated_at
    response_code
    ip_address
  }
}

query GetIPDetails {
  getIPDetails(input: "127.0.0.1") {
    uuid
    created_at
    updated_at
    response_code
    ip_address
  }
}
```

The current version of the schema can be found in [`gitlab.com/davidheadrick93/spamhaus-api/-/blob/master/graph/schema.graphqls`](https://gitlab.com/davidheadrick93/spamhaus-api/-/blob/master/graph/schema.graphqls)

# Setting Up Your Own Database
By Default this repo comes with a test database in `./test/test.db`.

If you would like to set up your own database do the following:

```bash
#create a db on your local machine
╰─$ touch example.db
```

```bash
#open sqlite
╰─$ sqlite3
```

You should see the following:
```bash
╰─$ sqlite3
SQLite version 3.31.1 2020-01-27 19:55:54
Enter ".help" for usage hints.
Connected to a transient in-memory database.
Use ".open FILENAME" to reopen on a persistent database.
sqlite>
```

Now copy and paste the following:

```bash
CREATE TABLE `ipAddresses` (
    `uuid` VARCHAR(64) PRIMARY KEY,
    `created_at` VARCHAR(64),
    `updated_at` VARCHAR(64),
    `response_code` VARCHAR(64),
    `ip_address` VARCHAR(64)
);
```
The application will look to the environment for a db file if the environment variable `DB_FILE` is set:

```bash
╰─$ export DB_FILE=/path/to/my/db/file.db
```

# Running With Docker
A Dockerfile is provided in the repository. It has a default port of 8080 and a default database of `./test/test.db`.

To build docker image:

```bash
╰─$ docker image build -t your-ip-image-name:latest .
```

## Run docker with defaults
To run docker container with default port and default database:

```bash
╰─$ docker container run -d -p 8080:8080 --name your-container-name-here your-image-name-here
```

Then visit [`gitlab.com/davidheadrick93/spamhaus-api/-/blob/master/graph/schema.graphqls`](https://gitlab.com/davidheadrick93/spamhaus-api/-/blob/master/graph/schema.graphqls) to use the application!

## Run docker with custom database
To run docker container with custom database:

```bash
╰─$ docker container run -d -p 8080:8080 -v /path/to/your/dbfile.db:/db/yourdbfile.db -e DB_FILE=/db/yourdbfile.db PORT=3333 --name your-container-name-here your-image-name-here
```