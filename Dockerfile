FROM golang:1.16 as builder

WORKDIR /go/src/gitlab.com/davidheadrick93/spamhaus-api

COPY go.* ./
RUN go mod download
COPY . .

RUN GOOS=linux go build -a -ldflags="-extldflags=-static" -o spamhaus-api server.go
RUN adduser                     \
    --disabled-password         \
    --gecos ""                  \
    --home "/nonexistent"       \
    --shell "/sbin/nologin"     \
    --uid "10000"               \
    "user"

FROM scratch

COPY --from=builder /go/src/gitlab.com/davidheadrick93/spamhaus-api/spamhaus-api /spamhaus-api

ENV PORT=8080
ENV DB_FILE /test/test.db

USER user

COPY --from=builder /etc/group /etc/group
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder --chown=user /go/src/gitlab.com/davidheadrick93/spamhaus-api/test/test.db /test/test.db

EXPOSE ${PORT}

CMD ["/spamhaus-api"]
