package db

import (
	"context"
	"database/sql"
	"log"
	"time"

	"gitlab.com/davidheadrick93/spamhaus-api/graph/model"
)

type IPAddressDB interface {
	Create(ipEntry *model.IP) error
	Read(ipAddress string) (*model.IP, error)
	Update(ipEntry *model.IP) (*model.IP, error)
	Delete(ipAddress string) error
}

type ipAddressDB struct {
	db *sql.DB
}

func NewIPAddressDBConnection(db *sql.DB) IPAddressDB {
	return &ipAddressDB{db: db}
}

func (ip *ipAddressDB) Create(ipEntry *model.IP) error {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	query := "INSERT INTO ipAddresses (uuid, created_at, updated_at, response_code, ip_address) values(?,?,?,?,?)"

	stmt, err := ip.db.PrepareContext(ctx, query)
	if err != nil {
		log.Printf("Error: %s", err.Error())
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, ipEntry.UUID, ipEntry.CreatedAt, ipEntry.UpdatedAt, ipEntry.ResponseCode, ipEntry.IPAddress)
	if err != nil {
		log.Printf("error creating db entry: %s", err.Error())
		return err
	}

	return err
}

func (ip *ipAddressDB) Read(ipAddress string) (*model.IP, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	ipInfo := &model.IP{}

	err := ip.db.QueryRowContext(ctx, "SELECT * FROM ipAddresses WHERE ip_address = ?", ipAddress).Scan(
		&ipInfo.UUID, &ipInfo.CreatedAt, &ipInfo.UpdatedAt, &ipInfo.ResponseCode, &ipInfo.IPAddress,
	)
	if err != nil {
		log.Println("error: "+err.Error(), " | ipInfo: ", ipInfo)
		return nil, err
	}

	return ipInfo, nil
}

func (ip *ipAddressDB) Update(ipEntry *model.IP) (*model.IP, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	query := "UPDATE ipAddresses SET updated_at=?, response_code=? WHERE ip_address=?"

	stmt, err := ip.db.PrepareContext(ctx, query)
	if err != nil {
		log.Printf("error: %s", err.Error())
		return nil, err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, time.Now().Format("2006-01-02 15:04"), ipEntry.ResponseCode,
		ipEntry.IPAddress,
	)
	if err != nil {
		log.Printf("error updating: %s", err.Error())
		return nil, err
	}

	updatedEntry, err := ip.Read(ipEntry.IPAddress)
	if err != nil {
		log.Printf("Error reading ipEntry from db: %s", err.Error())
		return nil, err
	}

	return updatedEntry, nil

}

func (ip *ipAddressDB) Delete(ipAddress string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	query := "DELETE FROM ipAddresses WHERE ip_address=?"

	stmt, err := ip.db.PrepareContext(ctx, query)
	if err != nil {
		log.Printf("error: %s", err.Error())
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, ipAddress)
	if err != nil {
		log.Printf("error deleting input: %s | %s", ipAddress, err.Error())
		return err
	}

	return nil
}
