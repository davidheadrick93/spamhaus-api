package db

import (
	"database/sql"
	"errors"
	"log"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/davidheadrick93/spamhaus-api/graph/model"
)

var testConnection *sql.DB

var ipTest = &model.IP{
	UUID:         uuid.New().String(),
	CreatedAt:    time.Now().AddDate(-1, 0, 0).Format("2006-01-02 15:04"),
	UpdatedAt:    time.Now().AddDate(-1, 0, 0).Format("2006-01-02 15:04"),
	ResponseCode: "good response",
	IPAddress:    "test ip",
}

func TestCreate(t *testing.T) {
	tt := []struct {
		input    *model.IP
		output   *model.IP
		testType string
		dbFile   string
	}{
		{ipTest, ipTest, "good test", "../test/test.db"},
		{ipTest, ipTest, "bad test", "../test/fail.db"},
	}

	for _, tc := range tt {
		log.Println("Now testing with input: ", tc.dbFile)

		testConnection = Connect(tc.dbFile)
		defer testConnection.Close()

		clearDB(testConnection)

		testDB := NewIPAddressDBConnection(testConnection)

		err := testDB.Create(ipTest)
		if tc.testType == "bad test" {
			assert.Error(t, err)
		} else {
			assert.Nil(t, err)
		}
	}
}

func TestRead(t *testing.T) {
	tt := []struct {
		input  *model.IP
		output *model.IP
		err    error
	}{
		{ipTest, ipTest, nil},
		{&model.IP{UUID: uuid.New().String(), CreatedAt: time.Now().Format("2006-01-02 15:04"),
			UpdatedAt: time.Now().Format("2006-01-02 15:04"), ResponseCode: "bad test", IPAddress: "bad test"},
			nil,
			sql.ErrNoRows},
	}

	for _, tc := range tt {
		log.Println("Now testing with input: ", tc.input)
		testConnection = Connect("../test/test.db")
		defer testConnection.Close()

		clearDB(testConnection)

		testDB := NewIPAddressDBConnection(testConnection)

		if tc.input.IPAddress != "bad test" {
			err := testDB.Create(tc.input)
			assert.Nil(t, err)
		}

		output, err := testDB.Read(tc.input.IPAddress)
		assert.Equal(t, tc.output, output)
		assert.Equal(t, tc.err, err)
	}
}

func TestUpdate(t *testing.T) {
	tt := []struct {
		input  *model.IP
		dbFile string
		err    error
	}{
		{ipTest, "../test/test.db", nil},
		{&model.IP{UUID: uuid.New().String(), CreatedAt: time.Now().Format("2006-01-02 15:04"),
			UpdatedAt: time.Now().Format("2006-01-02 15:04"), ResponseCode: "bad test", IPAddress: "bad test"},
			"../test/test.db",
			sql.ErrNoRows},
		{ipTest, "../test/fail.db", errors.New("bad test")},
	}

	for _, tc := range tt {
		log.Println("Now testing with input: ", tc.input)
		testConnection = Connect(tc.dbFile)
		defer testConnection.Close()

		clearDB(testConnection)

		testDB := NewIPAddressDBConnection(testConnection)

		if tc.input.IPAddress != "bad test" {
			err := testDB.Create(ipTest)
			if tc.dbFile == "../test/fail.db" {
				assert.Error(t, err)
			} else {
				assert.Nil(t, err)
			}
		}

		output, err := testDB.Update(ipTest)
		if tc.dbFile != "../test/fail.db" {
			assert.Equal(t, tc.err, err)
		} else {
			assert.Error(t, err)
		}
		if tc.err == nil {
			assert.Greater(t, output.UpdatedAt, ipTest.UpdatedAt)
		} else {
			assert.Nil(t, output)
		}
	}
}

func TestDelete(t *testing.T) {
	tt := []struct {
		input  *model.IP
		dbFile string
	}{
		{ipTest, "../test/test.db"},
		{ipTest, "../test/fail.db"},
	}

	for _, tc := range tt {
		testConnection = Connect(tc.dbFile)
		defer testConnection.Close()

		clearDB(testConnection)

		testDB := NewIPAddressDBConnection(testConnection)

		err := testDB.Create(ipTest)
		if tc.dbFile == "../test/fail.db" {
			assert.Error(t, err)
		} else {
			assert.Nil(t, err)
		}

		err = testDB.Delete(ipTest.IPAddress)
		if tc.dbFile == "../test/fail.db" {
			assert.Error(t, err)
		} else {
			assert.Nil(t, err)
		}
	}
}

func clearDB(db *sql.DB) {
	db.Exec("DELETE from ipAddresses")
}
