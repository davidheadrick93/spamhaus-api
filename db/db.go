package db

import (
	"database/sql"
	"log"

	_ "github.com/mattn/go-sqlite3"
)

func Connect(filename string) *sql.DB {
	log.Println("Opening DBFile: " + filename)

	db, err := sql.Open("sqlite3", filename)
	if err != nil {
		log.Fatalf("Error opening the specified database file: %s | Error: %s", filename, err.Error())
	}

	return db
}
