package main

import (
	"net/http"
	"os"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/davidheadrick93/spamhaus-api/db"
	"gitlab.com/davidheadrick93/spamhaus-api/graph"
	"gitlab.com/davidheadrick93/spamhaus-api/graph/generated"
)

const defaultPort = "8080"
const defaultDB = "./test/test.db"

func main() {
	dbFile := os.Getenv("DB_FILE")
	if dbFile == "" {
		dbFile = defaultDB
	}
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	dbConn := db.Connect(dbFile)
	defer dbConn.Close()

	r := chi.NewRouter()

	r.Use(middleware.BasicAuth("simple", map[string]string{"secureworks": "supersecret"}))
	r.Use(middleware.RequestID)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{
		Resolvers: &graph.Resolver{DB: db.NewIPAddressDBConnection(dbConn)}}))

	r.Mount("/", graph.HealthCheckRouter())
	r.Mount("/graphql", playground.Handler("GraphQL playground", "/query"))
	r.Mount("/query", srv)

	http.ListenAndServe(":"+port, r)
}
