CREATE TABLE `ipAddresses` (
    `uuid` VARCHAR(64) PRIMARY KEY,
    `created_at` VARCHAR(64),
    `updated_at` VARCHAR(64),
    `response_code` VARCHAR(64),
    `ip_address` VARCHAR(64)
);