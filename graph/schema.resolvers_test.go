package graph

import (
	"database/sql"
	"errors"
	"log"
	"testing"
	"time"

	"github.com/99designs/gqlgen/client"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/davidheadrick93/spamhaus-api/graph/generated"
	"gitlab.com/davidheadrick93/spamhaus-api/graph/model"
	mock "gitlab.com/davidheadrick93/spamhaus-api/mocks"
)

var testInput = &model.IP{
	UUID:         "49776272-7f60-4d52-9390-0173fccdb1fb",
	CreatedAt:    time.Now().Format("2006-01-02 15:04"),
	UpdatedAt:    time.Now().Format("2006-01-02 15:04"),
	ResponseCode: "IP Address not listed on Spamhaus!",
	IPAddress:    "127.0.0.1",
}

func TestEnqueue(t *testing.T) {
	ctrl := gomock.NewController(t)

	mockDB := mock.NewMockIPAddressDB(ctrl)

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{
		Resolvers: &Resolver{DB: mockDB},
	}))
	c := client.New(srv)

	t.Run("create entry", func(t *testing.T) {
		mockDB.EXPECT().Read(testInput.IPAddress).AnyTimes().Return(nil, sql.ErrNoRows)
		mockDB.EXPECT().Create(gomock.Any()).AnyTimes().Return(nil)

		var resp struct {
			Enqueue []string
		}

		c.MustPost(`mutation m($ipAddress: [String!]!) {
					enqueue(input: $ipAddress)
		}`, &resp, client.Var("ipAddress", []string{"127.0.0.1"}))

		log.Println("POSTing mutation enqueue | resp: ", resp)

		assert.Equal(t, struct{ Enqueue []string }{Enqueue: []string{"input: 127.0.0.1 response code(s): IP Address not listed on Spamhaus!"}},
			resp)
	})

	t.Run("invalid input", func(t *testing.T) {
		var resp struct {
			Enqueue []string
		}

		err := c.Post(`mutation m($ipAddress: [String!]!) {
					enqueue(input: $ipAddress)
				}`, &resp, client.Var("ipAddress", []string{"invalid ip"}))

		log.Println("POSTing mutation enqueue: resp | err: ", resp, err.Error())

		assert.Equal(t, struct{ Enqueue []string }{Enqueue: []string(nil)}, resp)
		assert.Equal(t, "[{\"message\":\"invalid input: invalid ip\",\"path\":[\"enqueue\"]}]", err.Error())
	})
}

func TestUpdateEnqueue(t *testing.T) {
	ctrl := gomock.NewController(t)

	mockDB := mock.NewMockIPAddressDB(ctrl)

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{
		Resolvers: &Resolver{DB: mockDB},
	}))
	c := client.New(srv)

	t.Run("update entry", func(t *testing.T) {
		mockDB.EXPECT().Read(testInput.IPAddress).AnyTimes().Return(testInput, nil)
		mockDB.EXPECT().Update(testInput).AnyTimes().Return(testInput, nil)

		var resp struct {
			Enqueue []string
		}

		c.MustPost(`mutation m($ipAddress: [String!]!) {
					enqueue(input: $ipAddress)
		}`, &resp, client.Var("ipAddress", []string{"127.0.0.1"}))

		log.Println("POSTing mutation enqueue | resp: ", resp)

		assert.Equal(t, struct{ Enqueue []string }{Enqueue: []string{"input: 127.0.0.1 response code(s): IP Address not listed on Spamhaus!"}},
			resp)
	})
}

func TestGetIPDetails(t *testing.T) {
	ctrl := gomock.NewController(t)

	mockDB := mock.NewMockIPAddressDB(ctrl)

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{
		Resolvers: &Resolver{DB: mockDB},
	}))
	c := client.New(srv)

	t.Run("successfully get ip details from db", func(t *testing.T) {
		mockDB.EXPECT().Read(testInput.IPAddress).AnyTimes().Return(testInput, nil)

		var resp struct {
			GetIPDetails model.IP
		}

		c.MustPost(`query q($ipAddress: String!) {
			getIPDetails(input: $ipAddress) {
					uuid
					created_at
					updated_at
					response_code
					ip_address
				}
			}`, &resp, client.Var("ipAddress", testInput.IPAddress))

		log.Println("POSTing query getDeckDetails | resp: ", resp)

		assert.Equal(t, testInput.UUID, resp.GetIPDetails.UUID)
		assert.Equal(t, testInput.CreatedAt, resp.GetIPDetails.CreatedAt)
		assert.Equal(t, testInput.UpdatedAt, resp.GetIPDetails.UpdatedAt)
		assert.Equal(t, testInput.ResponseCode, resp.GetIPDetails.ResponseCode)
		assert.Equal(t, testInput.IPAddress, resp.GetIPDetails.IPAddress)
	})

	t.Run("invalid input for getIPDetails", func(t *testing.T) {
		mockDB.EXPECT().Read("invalid input").AnyTimes().Return(nil, errors.New("invalid ip address"))

		var resp struct {
			GetIPDetails model.IP
		}

		err := c.Post(`query q($ipAddress: String!) {
					getIPDetails(input: $ipAddress) {
						uuid
						created_at
						updated_at
						response_code
						ip_address
					}
				}`, &resp, client.Var("ipAddress", "invalid input"))

		log.Println("POSTing query getDeckDetails | resp: ", resp)

		assert.Equal(t, "[{\"message\":\"invalid ip address\",\"path\":[\"getIPDetails\"]}]", err.Error())
	})

}
