package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"errors"
	"log"
	"time"

	"github.com/google/uuid"
	"gitlab.com/davidheadrick93/spamhaus-api/graph/generated"
	"gitlab.com/davidheadrick93/spamhaus-api/graph/model"
	"gitlab.com/davidheadrick93/spamhaus-api/helper"
)

func (r *mutationResolver) Enqueue(ctx context.Context, input []string) ([]string, error) {
	var respArray []string
	var respErr error
	for i := 0; i < len(input); i++ {
		validInput := helper.ValidateIPAddress(input[i])
		if validInput {
			//if input is valid, check db to see if it exist
			DBEntry, err := r.DB.Read(input[i])
			//if it does not, look it up and create entry
			if err != nil {
				addr := helper.ReverseIP(input[i])
				log.Println("addr: ", addr)
				res := helper.LookUpIPAddress(addr)
				log.Println("res: ", res)

				newDBEntry := &model.IP{
					UUID:         uuid.New().String(),
					CreatedAt:    time.Now().Format("2006-01-02 15:04"),
					UpdatedAt:    time.Now().Format("2006-01-02 15:04"),
					ResponseCode: res,
					IPAddress:    input[i],
				}

				log.Println("newDBEntry: ", newDBEntry)
				r.DB.Create(newDBEntry)

				respArray = append(respArray, "input: "+input[i]+" response code(s): "+res)
				log.Println("respArray: ", respArray)
				respErr = nil
			} else { //if it exists, look it up again and update entry
				addr := helper.ReverseIP(input[i])
				res := helper.LookUpIPAddress(addr)

				DBEntry.ResponseCode = res

				r.DB.Update(DBEntry)

				respArray = append(respArray, "input: "+input[i]+" response code(s): "+res)
				respErr = nil
			}
		} else {
			//invalid input error
			respArray = append(respArray, "input: "+input[i]+" is invalid!")
			respErr = errors.New("invalid input: " + input[i])
		}
	}

	return respArray, respErr
}

func (r *queryResolver) GetIPDetails(ctx context.Context, input string) (*model.IP, error) {
	validInput := helper.ValidateIPAddress(input)
	if validInput {
		return r.DB.Read(input)
	}
	return nil, errors.New("invalid ip address")
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
