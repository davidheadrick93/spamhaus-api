package graph

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi/v5"
	"github.com/stretchr/testify/assert"
)

func TestHealthCheckRouter(t *testing.T) {
	r := chi.NewRouter()
	r.Mount("/", HealthCheckRouter())
	assert.NotNil(t, r)
}

func TestHealthCheck(t *testing.T) {
	ctx := chi.NewRouteContext()
	req, err := http.NewRequest(http.MethodGet, "/", nil)
	assert.NoError(t, err)
	req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, ctx))

	resp := httptest.NewRecorder()

	r := chi.NewRouter()
	r.Get("/", healthCheck)
	r.ServeHTTP(resp, req)

	assert.Equal(t, resp.Code, http.StatusOK)
	assert.Equal(t, `{"health status":"healthy"}`, resp.Body.String())
}
