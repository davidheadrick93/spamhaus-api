package graph

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-chi/chi"
)

func HealthCheckRouter() *chi.Mux {
	r := chi.NewRouter()

	r.Get("/", healthCheck)
	return r
}

func healthCheck(w http.ResponseWriter, r *http.Request) {
	respondWithJSON(w, http.StatusOK, map[string]string{"health status": "healthy"})
}

func respondWithJSON(w http.ResponseWriter, statusCode int, payload interface{}) error {
	message, err := json.Marshal(payload)
	if err != nil {
		log.Printf("unable to marshal JSON response: %s", err.Error())
		return err
	}
	w.WriteHeader(statusCode)
	w.Write(message)

	return nil
}
