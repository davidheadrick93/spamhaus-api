// Code generated by MockGen. DO NOT EDIT.
// Source: db/db_funcs.go

// Package mock is a generated GoMock package.
package mock

import (
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
	model "gitlab.com/davidheadrick93/spamhaus-api/graph/model"
)

// MockIPAddressDB is a mock of IPAddressDB interface.
type MockIPAddressDB struct {
	ctrl     *gomock.Controller
	recorder *MockIPAddressDBMockRecorder
}

// MockIPAddressDBMockRecorder is the mock recorder for MockIPAddressDB.
type MockIPAddressDBMockRecorder struct {
	mock *MockIPAddressDB
}

// NewMockIPAddressDB creates a new mock instance.
func NewMockIPAddressDB(ctrl *gomock.Controller) *MockIPAddressDB {
	mock := &MockIPAddressDB{ctrl: ctrl}
	mock.recorder = &MockIPAddressDBMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockIPAddressDB) EXPECT() *MockIPAddressDBMockRecorder {
	return m.recorder
}

// Create mocks base method.
func (m *MockIPAddressDB) Create(ipEntry *model.IP) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Create", ipEntry)
	ret0, _ := ret[0].(error)
	return ret0
}

// Create indicates an expected call of Create.
func (mr *MockIPAddressDBMockRecorder) Create(ipEntry interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Create", reflect.TypeOf((*MockIPAddressDB)(nil).Create), ipEntry)
}

// Delete mocks base method.
func (m *MockIPAddressDB) Delete(ipAddress string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Delete", ipAddress)
	ret0, _ := ret[0].(error)
	return ret0
}

// Delete indicates an expected call of Delete.
func (mr *MockIPAddressDBMockRecorder) Delete(ipAddress interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Delete", reflect.TypeOf((*MockIPAddressDB)(nil).Delete), ipAddress)
}

// Read mocks base method.
func (m *MockIPAddressDB) Read(ipAddress string) (*model.IP, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Read", ipAddress)
	ret0, _ := ret[0].(*model.IP)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Read indicates an expected call of Read.
func (mr *MockIPAddressDBMockRecorder) Read(ipAddress interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Read", reflect.TypeOf((*MockIPAddressDB)(nil).Read), ipAddress)
}

// Update mocks base method.
func (m *MockIPAddressDB) Update(ipEntry *model.IP) (*model.IP, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Update", ipEntry)
	ret0, _ := ret[0].(*model.IP)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Update indicates an expected call of Update.
func (mr *MockIPAddressDBMockRecorder) Update(ipEntry interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Update", reflect.TypeOf((*MockIPAddressDB)(nil).Update), ipEntry)
}
