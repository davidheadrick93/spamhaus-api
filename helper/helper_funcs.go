package helper

import (
	"log"
	"net"
	"strings"
)

func ValidateIPAddress(input string) bool {
	validIP := net.ParseIP(input)
	return validIP != nil
}

func ReverseIP(input string) string {
	splitIP := strings.Split(input, ".")
	j := len(splitIP) - 1
	for i := 0; i < len(splitIP)/2; i++ {
		splitIP[i], splitIP[j-i] = splitIP[j-i], splitIP[i]
	}

	reverseIP := strings.Join(splitIP, ".")
	return reverseIP
}

func LookUpIPAddress(input string) string {
	res, err := net.LookupHost(input + ".zen.spamhaus.org")
	if err != nil {
		if err.(*net.DNSError).IsNotFound {
			log.Printf("IP Address not listed on Spamhaus!: %s | %s", input, err)
			return "IP Address not listed on Spamhaus!"
		}
		return "Error"
	}

	output := strings.Join(res, ",")
	return output
}
